# bumper

bumper is a docker image that automatically tags and increments versions 
on a Git repository using the [semver](https://semver.org)
specification.

For incrementing the major, minor or patch versions, you can use
`[bump_major]`, `[bump_minor]` or `[bump_patch]` in a commit message, respectively. Per
default, Bumper increments the patch version on the branch on which it is
configured to run (usually `master`). 

bumper generates changelog entries that matches the format
described at [Keep a Changelog](https://keepachangelog.com/en/1.0.0/) for minor
and major version bumps. bumper automatically searches through the commit messages for tagged
changelog entries. At the moment, the following changelog tags are supported:

- **added** for new features.
- **changed** for changes in existing functionality.
- **deprecated** for soon-to-be removed features.
- **removed** for now removed features.
- **fixed** for any bug fixes.
- **security** in case of vulnerabilities.

You can tag changelog entries by providing the changelog tag a git comment:

```
[<tag>]{<description>}
```

where `<tag>` can be one of the tags mentioned above, and `<description>` is
the actual text that would be added to the changelog and that has to match the regular expression `[\\(\\)a-zA-Z0-9\\.: ]+`.

In case you want to mix different commands/changelog-tags in one commit message, they have to be separated with newline
characters.

## Configuration

bumper needs write access to your git project in order to automatically add new
tags. This is done by means of SSH authentication. So for using bumper in you
GitLab project, you first have to generate a SSH key by invoking the command
below.

``` bash
ssh-keygen -t rsa -b 4096 -C "<your_email>" -f /tmp/key
```

Afterwards, you can add the public component to your repository in the GitLab
configuration under `Settings->Repository->Deploy Keys`. Please make sure that
`Write access allowed` is enabled.

The private key can be added under `Settings->CI/CD->Variables`. For the
sample configuration below, we named the environment variable `BUMPER_SSH`. So
you could add a variable of the type `Variable`, with the key `BUMPER_SSH` and
paste the private component of the SSH key into the `Value` field. 

Afterwards, you can add the sample configuration below to your `.gitlab-ci.yml`
configuration file; please replace the placeholder `<you_project_url>` with
your GitLab project URL. Note that in the line `git clone
git@gitlab.com:<your_project_url>.git` we are cloning your repository using SSH
so that bumper has the proper permission to add tags to your repository.

This configuration will enable bumper on the master branch of your project.
Please note that bumper only starts tagging, once there is already a baseline
version in place; so if you want to enable bumper, you could tag the `HEAD` of your
master branch in the following way:

```
git tag version_latest HEAD
git tag v0.0.0 HEAD
git push --tags
```

``` yaml
test:
  image: registry.gitlab.com/julianthome/bumper:master-v1.0.1
  stage: bump
  variables:
    GIT_STRATEGY: none
  before_script:
    - mkdir -p ~/.ssh && chmod 700 ~/.ssh
    - ssh-keyscan gitlab.com >> ~/.ssh/known_hosts && chmod 644 ~/.ssh/known_hosts
    - eval $(ssh-agent -s)
    - ssh-add <(echo "$BUMPER_SSH")
    - git clone git@gitlab.com:<your_project_url>.git
  script: 
    - bump.rb -v "v" -c "CHANGELOG.md" -g test -n "bumpbot" -e "bumpbot@example.com"
  resource_group: bump
  only:
      refs:
        - master
  except:
      - tags
```
